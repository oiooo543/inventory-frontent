import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import {IProduct} from '../model/product.model';
import {createRequestOption} from './request-util';
import {environment} from '../../environments/environment';
import {IProductlist} from '../model/productlist.model';



type EntityResponseType = HttpResponse<IProduct>;
type EntityArrayResponseType = HttpResponse<IProduct[]>;

@Injectable({ providedIn: 'root' })
export class ProductService {
  public resourceUrl = environment.link + 'product';

  constructor(protected http: HttpClient) {}

  create(courses: IProduct): Observable<EntityResponseType> {
    return this.http.post<IProduct>(this.resourceUrl, courses, { observe: 'response' });
  }

  update(product: IProduct): Observable<EntityResponseType> {
    return this.http.patch<IProduct>(`${this.resourceUrl}/${product.id}`, product, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IProduct>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<HttpResponse<IProductlist>> {
    const options = createRequestOption(req);
    return this.http.get<IProductlist>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  queryAtUrl(url: string): Observable<HttpResponse<IProductlist>> {
    return this.http.get<IProductlist>(url, { observe: 'response' });
  }
}
