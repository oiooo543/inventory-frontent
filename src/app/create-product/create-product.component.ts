import { Component, OnInit } from '@angular/core';
import {IProduct} from '../model/product.model';
import {Observable} from 'rxjs';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {ProductService} from '../services/product.service';

import {NgForm} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent implements OnInit {

  isSaving: boolean;
  product: IProduct;
  name: string;
  qty: number;
  price: number;
  constructor(private productService: ProductService, private toastr: ToastrService) { }

  ngOnInit() {

    this.isSaving = false;
  }

  resetState() {

  }

  save(formValues: NgForm) {
    this.isSaving = true;
    this.product = formValues.value;
    if (this.product.id !== undefined) {
      this.subscribeToSaveResponse(this.productService.update(this.product));
    } else {
      this.subscribeToSaveResponse(this.productService.create(this.product));
    }
    formValues.reset();
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProduct>>) {
    result.subscribe((res: HttpResponse<IProduct>) => this.onSaveSuccess(),
      (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = true;
    this.toastr.success('Your product have been saved successfully', 'Successful');
  }

  protected onSaveError() {
    this.isSaving = false;
    this.toastr.error('Save failed, be sure the product with the name does not already exist', 'Save failed');
  }

}
