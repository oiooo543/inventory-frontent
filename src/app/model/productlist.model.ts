import {IProduct} from './product.model';

export interface IProductlist {

  current_page?: number;
  data?: IProduct[];
  first_page_url?: string;
  from?: number;
  last_page?: number;
  last_page_url?: string;
  next_page_url?: string;
  path?: string;
  per_page?: number;
  prev_page_url?: string;
  to?: 8;
  total?: 8;

}

export class Productlist implements IProductlist {
  constructor(
    // tslint:disable-next-line:variable-name
    current_page?: number,
    data?: IProduct[],
    // tslint:disable-next-line:variable-name
    first_page_url?: string,
    from?: number,
    // tslint:disable-next-line:variable-name
    last_page?: number,
    // tslint:disable-next-line:variable-name
    last_page_url?: string,
    // tslint:disable-next-line:variable-name
    next_page_url?: string,
    path?: string,
    // tslint:disable-next-line:variable-name
    per_page?: number,
    // tslint:disable-next-line:variable-name
    prev_page_url?: string,
    to?: 8,
    total?: 8,
  ) {}

}
