

export interface IProduct {
  id?: number;
  name?: string;
  price?: number;
  qty?: number;
  created_at?: number;
}

export class Product implements IProduct {
  constructor(
    id?: number,
    name?: string,
    price?: number,
    qty?: number,
    // tslint:disable-next-line:variable-name
    created_at?: number,
  ) {}

}
