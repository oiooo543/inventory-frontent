import { Component, OnInit } from '@angular/core';
import {IProduct} from '../model/product.model';
import {ProductService} from '../services/product.service';
import { filter, map } from 'rxjs/operators';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {ToastrService} from 'ngx-toastr';
import {IProductlist} from '../model/productlist.model';
import {NgForm} from '@angular/forms';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-product-table',
  templateUrl: './product-table.component.html',
  styleUrls: ['./product-table.component.css']
})
export class ProductTableComponent implements OnInit {

  productlist: IProduct[];
  // tslint:disable-next-line:variable-name
  _listfilter: string;
  filteredList: IProduct[];
  details: IProductlist;
  // tslint:disable-next-line:variable-name
  next_page_url: string;

  isSaving: boolean;
  product: IProduct;
  name: string;
  qty: number;
  price: number;
  id: number;
  get listFilter(): string {
    return this._listfilter;
  }
  set listFilter(value: string) {
    this._listfilter = value;
    this.filteredList = this.listFilter ? this.performFilter(this.listFilter) : this.productlist;
  }
  constructor(private getProducts: ProductService, protected toastr: ToastrService) { }

  ngOnInit() {
    this.isSaving = false;
    this.extracted();
  }

  private extracted() {
    this.getProducts
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IProductlist>) => mayBeOk.ok),
        map((response: HttpResponse<IProductlist>) => response.body)
      )
      .subscribe((res: IProductlist) => {
          console.log(res.data);
          this.details = res;
          this.productlist = res.data;
          this.filteredList = this.productlist;
          this.next_page_url = res.next_page_url;
        },
        (res: HttpErrorResponse) => this.onError(res.message));
  }

  performFilter(filterBy: string) {
    filterBy = filterBy.toLocaleLowerCase();
    return this.productlist.filter((plist: IProduct) => plist.name.toLocaleLowerCase().indexOf(filterBy) !== -1);
  }

  onSelect(selectedItem: IProduct) {
    // console.log(selectedItem.product.name);
    this.id = selectedItem.id;
    this.name = selectedItem.name;
    this.price = selectedItem.price;
    this.qty = selectedItem.qty;
  }

  resetState() {

  }

  protected onError(errorMessage: string) {
    this.toastr.error(errorMessage, null, null);
  }

  prevPage() {
    this.extractedPage(this.details.prev_page_url);
  }

  nextPage() {
    this.extractedPage(this.details.next_page_url);
  }

  private extractedPage(urs: string) {
    this.getProducts
      .queryAtUrl(urs)
      .pipe(
        filter((mayBeOk: HttpResponse<IProductlist>) => mayBeOk.ok),
        map((response: HttpResponse<IProductlist>) => response.body)
      )
      .subscribe((res: IProductlist) => {
          console.log(res.data);
          this.details = res;
          this.productlist = res.data;
          this.filteredList = this.productlist;
          this.next_page_url = res.next_page_url;
        },
        (res: HttpErrorResponse) => this.onError(res.message));
  }

  save(formValues: NgForm) {
    this.isSaving = true;
    this.product = formValues.value;
    this.subscribeToSaveResponse(this.getProducts.update(this.product));
    formValues.reset();
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IProduct>>) {
    result.subscribe((res: HttpResponse<IProduct>) => this.onSaveSuccess(),
      (res: HttpErrorResponse) => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = true;
    this.toastr.success('Your product update successfully', 'Successful');
  }

  protected onSaveError() {
    this.isSaving = false;
    this.toastr.error('Update failed');
  }


}
